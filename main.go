package main

import (
	"fmt"

	"gitlab.com/fhdo-ewks2018/go/characterservice/db"
	"gitlab.com/fhdo-ewks2018/go/characterservice/service"
)

const appName = "characterservice"

func main() {
	err := db.OpenConnection("")
	if err != nil {
		fmt.Errorf("Unable to open database\n Error: %v", err.Error())
		return
	}
	defer db.CloseConnection()

	fmt.Printf("Starting service: %v\n", appName)
	service.StartHTTPServer("33333")

}
