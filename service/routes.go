package service

import "net/http"

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

var routes = Routes{
	Route{
		"GetCharacter",
		"GET",
		"/characters/{charId}",
		GetCharacter,
	},
	Route{
		"PutCharacter",
		"PUT",
		"/characters",
		PutCharacter,
	},
	Route{
		"PostCharacter",
		"POST",
		"/characters",
		PostCharacter,
	},
}
