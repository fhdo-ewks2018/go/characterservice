package service

import (
	"log"
	"net/http"
	"strconv"
)

// StartHTTPServer starts the HTTP server serving the character microservice.
func StartHTTPServer(port string) {
	portNo, err := strconv.Atoi(port)
	if err != nil {
		log.Printf("Error: %v is not a valid port number", port)
		return
	}
	log.Println("Creating router")
	router := NewRouter()
	http.Handle("/", router)
	log.Printf("Starting HTTP server (port: %d)", portNo)
	err = http.ListenAndServe(":"+port, nil)
	if err != nil {
		log.Println("An error occured while starting the HTTP server")
		log.Println("Error: " + err.Error())
	}
}
