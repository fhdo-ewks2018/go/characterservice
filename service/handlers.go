package service

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/fhdo-ewks2018/go/characterservice/db"
)

func GetCharacter(w http.ResponseWriter, r *http.Request) {
	charId := mux.Vars(r)["charId"]
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Write([]byte("{\"result\":\"OK\",\n\"charId\":\"" + charId + "\"}"))
}

func PutCharacter(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	conn, err := db.GetConnection()
	if err == nil {
		w.Write([]byte("{\"result\":\"ERROR\",\n\"msg\":\"" + err.Error() + "\"}"))
	}
	id, err := conn.AddCharacter()
	if err != nil {
		w.Write([]byte("{\"result\":\"ERROR\",\n\"msg\":\"" + err.Error() + "\"}"))
	} else {
		w.Write([]byte("{\"result\":\"OK\",\n\"id\":\"" + id + "\"}"))
	}
}

func PostCharacter(w http.ResponseWriter, r *http.Request) {

}
