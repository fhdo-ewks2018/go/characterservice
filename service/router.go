package service

import (
	"log"

	"github.com/gorilla/mux"
)

// NewRouter creates a new router with the routes form the service.routes array installed.
func NewRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)

	for _, route := range routes {
		router.Methods(route.Method).Path(route.Pattern).Name(route.Name).Handler(route.HandlerFunc)
		log.Printf("Added route %v\n", route.Name)
	}

	return router
}
