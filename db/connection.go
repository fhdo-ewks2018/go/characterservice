package db

import "gitlab.com/fhdo-ewks2018/go/model"
import "sync"

type Connection struct {
	db        map[string]model.Character
	idCounter uint64
	mutex     sync.Mutex
}

var connection *Connection = nil

func OpenConnection(database string) error {
	connection = &Connection{make(map[string]model.Character), sync.Mutex{}}
	return nil
}

func CloseConnection() error {
	connection = nil
	return nil
}

func GetConnection() (*Connection, error) {
	if connection != nil {
		return connection, nil
	} else {
		return nil, &ConnectionError{"No database connection available"}
	}
}

type ConnectionError struct {
	msg string
}

func (e *ConnectionError) Error() string {
	return e.msg
}
