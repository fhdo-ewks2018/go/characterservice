package db

import "gitlab.com/fhdo-ewks2018/go/model"
import "github.com/globalsign/mgo/bson"
import "strconv"

func (conn *Connection) AddCharacter() (string, error) {
	conn.mutex.Lock()
	conn.idCounter += 1
	newId := conn.idCounter
	conn.mutex.Unlock()
	return strconv.FormatUint(newId, 10), nil
}

func (conn *Connection) ModifyCharacter(char *model.Character) error {
	conn.mutex.Lock()

	conn.mutex.Unlock()
	return nil
}

func (conn *Connection) FindCharacter(id bson.ObjectId) (*model.Character, error) {
	return nil, nil
}
